const enterPassword = document.querySelector(".enter-pw");
const confirmPassword = document.querySelector(".confirm-pw");
const passwordIcons = document.querySelectorAll(".icon-password");

passwordIcons.forEach((icon) => {
    icon.addEventListener("click", togglePasswordVisibility);
  });

function togglePasswordVisibility(event) {
    const inputField = event.target.previousElementSibling;
    const currentType = inputField.getAttribute("type");
  
    if (currentType === "password") {
      inputField.setAttribute("type", "text");
      event.target.classList.remove("fa-eye");
      event.target.classList.add("fa-eye-slash");
    } else {
      inputField.setAttribute("type", "password");
      event.target.classList.remove("fa-eye-slash");
      event.target.classList.add("fa-eye");
    }
  }


  document.querySelector("form").addEventListener("submit", function (event) {
    event.preventDefault();
    const passwordValue = enterPassword.value;
    const confirmPasswordValue = confirmPassword.value;
  
    if (passwordValue === confirmPasswordValue) {
      alert("You are welcome");
    } else {
      const errorMessage = document.createElement("div");
      errorMessage.textContent = "Потрібно ввести однакові значення";
      errorMessage.style.color = "red";
      document.querySelector("body").appendChild(errorMessage);
    }
  });